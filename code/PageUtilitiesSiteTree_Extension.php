<?php

/**
 * Class PageUtilitiesSiteTree_Extension
 *
 * Provides extra functionality to SiteTree classes for the page utilities
 */
class PageUtilitiesSiteTree_Extension extends DataExtension
{
    /**
     * Gets the Encoded Absolute URL of the extended object
     *
     * @return string
     */
    public function getEncodedAbsoluteURL()
    {
        return urlencode(Director::absoluteURL($this->owner->Link(), true));
    }

    /**
     * @see PageUtilitiesSiteTree_Extension::getEncodedAbsoluteURL()
     *
     * @return string
     */
    public function EncodedAbsoluteURL()
    {
        return $this->getEncodedAbsoluteURL();
    }

    /**
     * Gets the Encoded Title of the extended object
     *
     * @param int   $limit  Character limit
     * @return bool|string
     */
    public function getEncodedTitle($limit = 200)
    {
        $title = $this->owner->getField('Title');

        if (!$limit) {
            return urlencode($title);
        }

        // Limit string, encode, then limit again
        return substr(urlencode(substr($title, 0, $limit)), 0, $limit);
    }

    /**
     * @see PageUtilitiesSiteTree_Extension::getEncodedTitle()
     *
     * @param int $limit
     * @return bool|string
     */
    public function EncodedTitle($limit = 200)
    {
        return $this->getEncodedTitle($limit);
    }

    /**
     * Gets the Encoded Summary of the extended object
     *
     * @param int   $limit    Character Limit
     * @return bool|string
     */
    public function getEncodedSummary($limit = 1024)
    {
        $summary = $this->owner->getField('Intro')
            ? $this->owner->getField('Intro')
            : Convert::html2raw($this->owner->getField('Content'));

        if (!$limit) {
            return urlencode($summary);
        }

        // Limit string, encode, then limit again
        return substr(urlencode(substr($summary, 0, $limit)), 0, $limit);
    }

    /**
     * @see PageUtilitiesSiteTree_Extension::getEncodedSummary()
     *
     * @param int   $limit  Character Limit
     * @return bool|string
     */
    public function EncodedSummary($limit = 1024)
    {
        return $this->getEncodedSummary($limit);
    }

    /**
     * Gets the module directory
     *
     * @return string
     */
    public function getModuleDir()
    {
        return PAGEUTILS_DIR;
    }

    /**
     * @see PageUtilitiesSiteTree_Extension::getModuleDir()
     *
     * @return string
     */
    public function ModuleDir()
    {
        return $this->getModuleDir();
    }

    /**
     * Renders the Page Utils section
     *
     * @return string Rendered HTML
     */
    public function getPageUtils()
    {
        return $this->owner->renderWith('PageUtils');
    }

    /**
     * @see PageUtilitiesSiteTree_Extension::getPageUtils()
     *
     * @return string
     */
    public function PageUtils()
    {
        return $this->getPageUtils();
    }

    /**
     * Renders the Social Links section
     *
     * @return string Rendered HTML
     */
    public function getSocialLinks()
    {
        return $this->owner->renderWith('SocialLinks');
    }

    /**
     * @see PageUtilitiesSiteTree_Extension::getSocialLinks()
     *
     * @return string
     */
    public function SocialLinks()
    {
        return $this->getSocialLinks();
    }
}
