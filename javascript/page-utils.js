/**
 * Removes the hidden class from the print item and adds an event callback to print
 */
function initPrintUtility() {
  // Define variables
  var print =  document.querySelector('[data-page-utils-print-item]');
  var hiddenClass = 'hidden';

  // Remove hidden class
  if (print.classList) {
    print.classList.remove(hiddenClass);
  } else {
    print.className = print.className.replace(new RegExp('(^|\\b)' + hiddenClass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }

  // Add print function to click
  print.addEventListener('click', function (event) {
    event.preventDefault();
    window.print();
  });
}

// Document Ready
if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
  initPrintUtility();
} else {
  document.addEventListener('DOMContentLoaded', initPrintUtility);
}
