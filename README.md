# Page Utilities

## Overview
- Used on Parents and Education. 
- Provides social share links with encoded content as well as a print button. 
- Styles need to be defined in the theme (look at the template for class names).
- There is javascript which will remove the 'hidden' class from the print item.

## Installation
Add this to your composer.json file and run `composer update`

```json
"require": {
    ...
    "moe/page-utilities": "@stable",
    ...
}
```

```json
"repositories": [
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.cwp.govt.nz/modules/page-utilities.git",
        "private": "true"
    },
    ...
]
```

## Usage
Add the page utils to your template

```html
<div>
    $PageUtils
</div>
```

