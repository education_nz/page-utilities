<ul class="social-links">
    <li><a title="Facebook" data-width="600" data-height="320" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={$EncodedAbsoluteURL}" class="fb social-link"><span>facebook</span></a></li>
    <li><a title="Twitter" data-width="600" data-height="260" target="_blank" href="https://twitter.com/share?url={$EncodedAbsoluteURL}" class="twtr social-link"><span>twitter</span></a></li>
    <li><a title="LinkedIn" data-width="600" data-height="520" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={$EncodedAbsoluteURL}&title={$EncodedTitle}&summary={$EncodedSummary}" class="lkin social-link"><span>linkin</span></a></li>
    <li><a title="Google+" data-width="500" data-height="320" target="_blank" href="https://plus.google.com/share?url={$EncodedAbsoluteURL}" class="ggplus social-link"><span>google+</span></a></li>
</ul>